﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpFeatures
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start");

            ListPerformance.RunListVsHasSet();

            var people = new List<Person>{new Person{Age = 12, Name = "Smith"}, new Person{Age = 33, Name = "Oscar"}};

            Predicate<Person> oscarFinder = p => p.Name == "Oscar";
            Predicate<int> pre = a => a%2 == 0;

            var oscar = people.Find(oscarFinder);
            Console.WriteLine(oscar.ToString());
            Console.WriteLine("pre(1): {0}, pre(2): {1}", pre(1), pre(2));

            // last parameter is a return parameter, previous are arguments
            Func<bool, int> func = b => b ? 2 : 1;
            Func<bool, bool, int> func1 = (b, b1) => b && b1 ? 5 : 4;
            Console.WriteLine("func(true): {0}, func(false): {1}", func(true), func(false));
            Console.WriteLine("func1(true. true): {0}, func1(false, true): {1}", func1(true, true), func1(false, true));

            Console.WriteLine("Finish");
            Console.ReadKey();

        }
    }

    class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public override string ToString()
        {
            return string.Format("Name: {0}, Age: {1}", Name, Age);
        }
    }
}
