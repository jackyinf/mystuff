﻿using System;
using System.IO;
using System.Threading;
using System.Web.Mvc;

namespace ASpMvcSandbox.Controllers
{
    public class FileController : Controller
    {
        private const string Destination = @"C:\temp\kopio\test";

        public ActionResult Index()
        {
            return View();
        }

        public string SimpleText()
        {
            return "test";
        }

        public ActionResult Lightbox()
        {
            return View();
        }

        [HttpPost]
        public JsonResult UploadFilesparts(int flowChunkNumber, string flowFilename, int flowTotalChunks,
            int flowChunkSize, int flowCurrentChunkSize, int flowTotalSize)
        {
            // Thanks to HttpPost and ContentResult, file can be physically accessed 
            // through Request.Files, where file is the name of the input
            var file = Request.Files["file"];

            // Fix for reading bytes
            file.InputStream.Position = 0;

            // Read the bytes of the filepart (chunk)
            byte[] fileDataPart;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                fileDataPart = binaryReader.ReadBytes(file.ContentLength);
            }

            // write the chunk data onto the hard drive.
            FileStream fs = System.IO.File.Create(string.Format("{0}/{1}.chunk{2}", Destination, flowFilename, flowChunkNumber.ToString("000")));
            fs.Write(fileDataPart, 0, fileDataPart.Length);
            fs.Flush();
            fs.Close();

            // As a response, JSON is returned
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Merge chunks into a file. Search chunks by name
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public JsonResult CombineChunksForFile(string filename)
        {
            // Get all chunks for the specific filename
            var directoryInfo = new DirectoryInfo(Destination);
            var resultFileName = string.Format("{0}\\{1}", Destination, filename);
            var inputFiles = directoryInfo.GetFiles(string.Format("{0}.{1}*", filename, "chunk"));

            // create the file and copy bytes of the chunks into that file
            using (var createStream = System.IO.File.Create(resultFileName))
            {
                foreach (var file in inputFiles)
                {
                    using (var openReadStream = System.IO.File.OpenRead(file.FullName))
                    {
                        openReadStream.CopyTo(createStream);   
                    }
                    //System.IO.File.OpenRead(file.FullName).CopyTo(createStream);
                }
            }

            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Remove chunks of specified file. Chunks are searched by filename
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public JsonResult RemoveChunksForFile(string filename)
        {
            bool success = false;

            // It will attempt to remove chunks 5 times with interval of 1 second.
            for (int attempt = 0; attempt < 5; attempt++)
            {
                try
                {
                    // Remove all chunks
                    var directoryInfo = new DirectoryInfo(Destination);
                    var allChunks = directoryInfo.GetFiles(filename + ".chunk*");
                    foreach (var chunk in allChunks)
                    {
                        System.IO.File.Delete(chunk.FullName);
                    }
                    success = true;
                    break;
                }
                catch
                {
                    Thread.Sleep(2000);
                }
            }

            return Json(new { success }, JsonRequestBehavior.AllowGet);
        }

        #region not used

        /// <summary>
        /// Connects all the *.chunkXXX" files together into files by name.
        /// myfile.pdf.chunk001, myfile.pdf.chunk002... -> myfile.pdf, where in this example the name must be myfile.pdf.
        /// File's name is extracted from first chunk.
        /// </summary>
        /// <returns></returns>
        public JsonResult CombineChunks()
        {
            // Get only first chunks of all files
            var directoryInfo = new DirectoryInfo(Destination);
            var firstChunks = directoryInfo.GetFiles("*.chunk001");

            // loop through each unique name which is to be created
            foreach (var firstChunk in firstChunks)
            {
                using (var output = System.IO.File.Create(SliceName(firstChunk.FullName, ".")))
                {
                    // get all chunks of current filename
                    var inputFiles = directoryInfo.GetFiles(string.Format("{0}.{1}*", SliceName(firstChunk.Name, "."), "chunk"));
                    foreach (var file in inputFiles)
                    {
                        System.IO.File.OpenRead(file.FullName).CopyTo(output);
                    }
                }
            }

            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Removes all "*.chunkXXX" files.
        /// </summary>
        /// <returns></returns>
        public JsonResult RemoveChunks()
        {
            for (int attempt = 0; attempt < 5; attempt++)
            {
                try
                {
                    // Remove all chunks
                    var directoryInfo = new DirectoryInfo(Destination);
                    var allChunks = directoryInfo.GetFiles("*.chunk*");
                    foreach (var chunk in allChunks)
                    {
                        System.IO.File.Delete(chunk.FullName);
                    }
                    break;
                }
                catch
                {
                    Thread.Sleep(1000);
                }
            }

            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        /// <summary>
        /// Removes last part of the string.
        /// E.g. myfile.pdf -> myfile, if separator is '.'
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        private string SliceName(string filename, string separator)
        {
            var indexOfChunkseparator = filename.LastIndexOf(separator, StringComparison.Ordinal);
            return filename.Substring(0, indexOfChunkseparator);
        }
	}
}