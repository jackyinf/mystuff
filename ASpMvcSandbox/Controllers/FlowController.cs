﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace ASpMvcSandbox.Controllers
{
    public class FlowController : Controller
    {
        private const string Destination = @"C:\temp\kopio\test";

        [HttpPost]
        public ContentResult UploadFilesparts(int flowChunkNumber, string flowFilename, int flowTotalChunks,
            int flowChunkSize, int flowCurrentChunkSize, int flowTotalSize)
        {
            // Thanks to HttpPost and ContentResult, file can be physically accessed 
            // through Request.Files, where file is the name of the input
            var file = Request.Files["file"];

            // Fix for reading bytes
            file.InputStream.Position = 0;

            // Read the bytes of the filepart (chunk)
            byte[] fileDataPart;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                fileDataPart = binaryReader.ReadBytes(file.ContentLength);
            }

            // write the combined data onto the hard drive.
            FileStream fs = System.IO.File.Create(string.Format("{0}/{1}.chunk{2}", Destination, flowFilename, flowChunkNumber.ToString("000")));
            fs.Write(fileDataPart, 0, fileDataPart.Length);
            fs.Flush();
            fs.Close();

            // As a response, JSON is returned
            return Content("{ \"Success\" : \"true\"}");
        }

        /// <summary>
        /// Connects all the *.chunkXXX" files together into files by name.
        /// myfile.pdf.chunk001, myfile.pdf.chunk002... -> myfile.pdf, where in this example the name must be myfile.pdf.
        /// File's name is extracted from first chunk.
        /// </summary>
        /// <returns></returns>
        public JsonResult CombineChunks()
        {
            // Get only first chunks of all files
            var directoryInfo = new DirectoryInfo(Destination);
            var firstChunks = directoryInfo.GetFiles("*.chunk001");

            // loop through each unique name which is to be created
            foreach (var firstChunk in firstChunks)
            {
                using (var output = System.IO.File.Create(SliceName(firstChunk.FullName, ".")))
                {
                    // get all chunks of current filename
                    var inputFiles = directoryInfo.GetFiles(string.Format("{0}.{1}*", SliceName(firstChunk.Name, "."), "chunk"));
                    foreach (var file in inputFiles)
                    {
                        System.IO.File.OpenRead(file.FullName).CopyTo(output);
                        
                    }
                    output.Dispose();
                }
            }

            return Json(new {Success = true});
        }

        /// <summary>
        /// Removes all "*.chunkXXX" files.
        /// </summary>
        /// <returns></returns>
        public JsonResult RemoveChunks()
        {
            // Remove all chunks
            var directoryInfo = new DirectoryInfo(Destination);
            var allChunks = directoryInfo.GetFiles("*.chunk*");
            foreach (var chunk in allChunks)
            {
                System.IO.File.Delete(chunk.FullName);
            }

            return Json(new { Success = true });            
        }

        /// <summary>
        /// Removes last part of the string.
        /// E.g. myfile.pdf -> myfile, if separator is '.'
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        private string SliceName(string filename, string separator)
        {
            var indexOfChunkseparator = filename.LastIndexOf(separator, StringComparison.Ordinal);
            return filename.Substring(0, indexOfChunkseparator);
        }

    }
}