﻿/*global angular */
'use strict';

/**
 * The main app module
 * @name app
 * @type {angular.Module}
 */
var app = angular.module('app', ['flow'])
.config(['flowFactoryProvider', function (flowFactoryProvider) {

    flowFactoryProvider.defaults = {
        target: '/File/UploadFilesparts',
        permanentErrors: [404, 500, 501],
        maxChunkRetries: 1,
        chunkRetryInterval: 5000,
        simultaneousUploads: 4,
        query: {file: File}
    };

    flowFactoryProvider.on("fileSuccess", function ($file) {
        $.get("/File/CombineChunksForFile", { filename: $file.name }, function (data) {
            if (data.Success) {
                console.log($file.name + ": combination is done");
                console.log($file,name + ": cleaning up");
                $.get("/File/RemoveChunksForFile", { filename: $file.name }, function (result) {
                    if (result.success) {
                        console.log($file.name + ": cleanup is done");
                    } else {
                        console.log($file.name + ": cleanup failed");
                    }
                });
            }
        });
    });

}]);