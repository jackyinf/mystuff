﻿var myapp = angular.module('myapp', ['ngRoute']);

myapp.config(function($routeProvider) {
    $routeProvider.
        when('/app',
            {
                template: "<h1>{{model.message}}</h1><button ng-click='go()' ng-init='count=0'>Increment</button>",
                controller: "AppCtrl"
            })
        .when('/test',
            {
                template: "<h1>{{model.message}}</h1>",
                controller: "TestCtrl"
            })
        .otherwise({redirectTo: "/app"});
});

myapp.controller("AppCtrl", function($scope) {
    $scope.model = {
        message: "This is App Controller"
    };

    $scope.msg = function() {
        console.log('go');
    };
});

myapp.controller("TestCtrl", function($scope) {
    $scope.model = {
        message: "This is Test Controller"
    };
});

myapp.controller("FetchViewData", function($scope) {
    $scope.go = function() {
        console.log("go");
        simpleFunction();
    };
});

myapp.controller("AnotherController", function($scope) {

});
    
function simpleFunction() {
    console.log('simple function');
}