﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostgreHelpers
{
    public class CourseEntityAttributes
    {
        public string code { get; set; }
        public string description { get; set; }
    }
}
