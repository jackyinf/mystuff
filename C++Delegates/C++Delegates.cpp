// C++Delegates.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "iostream"

using namespace std;

struct Functor
{
	int operator()(double d)
	{
		return (int)(d + 1);
	}

	int operator+(int x)
	{
		return x - 5;
	}
};

struct DelegateList
{
	int f1(double d) { return (int)(d + 200); }
	int f2(double d) { return (int)(d + 100); }
};

int _tmain(int argc, _TCHAR* argv[])
{
	// Example 1
	Functor f;
	int a = f(3.14);
	int b = f + 6;
	cout << a << ", " << b << endl;

	// Example 2
	auto func = [](int i) -> double { return i * 2 / 2.15; };
	double c = func(1);
	cout << c << endl;

	// Example 3
	/*int fa(double d) { return 100; }
	typedef int(*MyFuncT) (double d);
	MyFuncT fp = &fa;
	int a = fp(3.14);*/

	// Example 4
	typedef int(DelegateList::*DelegateType)(double d);
	DelegateType d = &DelegateList::f1;
	DelegateList list;
	int eee = (list.*d)(3.14);
	cout << eee << endl;
	cin.get();
	return 0;
}