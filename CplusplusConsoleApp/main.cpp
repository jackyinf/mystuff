#include "stdafx.h"
#include "main.h"

void Main::Demo1()
{
	shared_ptr<Bar> bar1(new Bar());
	shared_ptr<Foo> foo1(new Foo("Foo1"));
	bar1->SetFoo(foo1);
	auto foo2 = bar1->GetFoo();
	foo1->SetName("Foo1_renamed");
	cout << foo2->GetName().c_str() << endl;
}

void Main::Demo2()
{
	shared_ptr<Sub1> sub1(new Sub1());

	//// this is especially cool - you can also use it in lists.
	//list< shared_ptr<int> > intList;
	//list< shared_ptr<IPrintable> > printableList;
	//for (int i = 0; i<100; ++i)
	//{
	//	intList.push_back(shared_ptr<int>(new int(rand())));
	//	printableList.push_back(shared_ptr<IPrintable>(new CPrintable(�list�)));
	//}
}

void Main::Demo3()
{
	shared_ptr<PeanutButter> pPeanutButter(new PeanutButter);
	shared_ptr<Jelly> pJelly(new Jelly);
	pPeanutButter->m_pJelly = pJelly;
	pJelly->m_pPeanutButter = pPeanutButter;
	// Both objects are leaked here�.
}

void Sub1::Go() {}
void Sub1::Run() {}