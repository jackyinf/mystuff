﻿// Demo 1

class Foo;
class Bar
{
public:
	Bar::Bar() { cout << "Bar created" << endl; };
	Bar::~Bar() { cout << "Bar deleted" << endl; }
	shared_ptr<Foo> Bar::GetFoo() { return m_foo; }
	void Bar::SetFoo(shared_ptr<Foo> foo) { m_foo = foo; }
private:
	shared_ptr<Foo> m_foo;
};

class Foo
{
public:
	Foo::Foo(char* name) { m_name = name; cout << "Foo created" << endl; };
	Foo::~Foo() { cout << "Foo deleted" << endl; };

	void Foo::SetName(char* name) { m_name = name; }
	string Foo::GetName() { return m_name; }
private:
	string m_name;
};

// Demo 2

class Base
{
public:
	virtual void Go(void) = 0;
	virtual void Run(void) = 0;
};

class Sub1 : Base
{
public:
	virtual void Go(void) override;
	virtual void Run(void) sealed;
};

// Demo 3

class Jelly;
class PeanutButter
{
public:
	shared_ptr<Jelly> m_pJelly;
	PeanutButter(void) { cout << "PeanutButter destructor\n"; }
};
class Jelly
{
public:
	shared_ptr<PeanutButter> m_pPeanutButter;
	Jelly(void) { cout << "Jelly destructor\n"; }
};

class Main
{
public:
	void Main::Demo1();
	void Main::Demo2();
	void Main::Demo3();
};