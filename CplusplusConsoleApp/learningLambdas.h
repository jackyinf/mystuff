#include <string>
#include <vector>
#include <functional>
using namespace std;

class AddressBook
{
public:
	AddressBook::AddressBook(void);
	vector<string> AddressBook::FindMatchingAddresses(function<bool(const string&)> func);
	vector<string> AddressBook::FindAddressesFromOrgs();
private:
	vector<string> m_addresses;
};

class EmailProcessor
{
public:
	EmailProcessor::EmailProcessor(void);
	void EmailProcessor::ReceiveMessage(string& message);
	void EmailProcessor::SetHandler(function <void(const string&)> handler_func);
private:
	function <void(const string&)> m_handler;
};