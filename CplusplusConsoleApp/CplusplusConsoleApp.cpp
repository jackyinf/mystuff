// CplusplusConsoleApp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
//#include "main.h"
#include "learningLambdas.h"

int _tmain(int argc, _TCHAR* argv[])
{
	//Main* main = new Main;
	//main->Demo2();

	float foo = 0;
	auto square = [&foo](float x) { foo = x * x; };
	square(2);
	cout << foo << endl;

	auto addressBook = new AddressBook();
	auto foundAddresses = addressBook->FindAddressesFromOrgs();

	auto processor = new EmailProcessor;
	processor->ReceiveMessage(string("John"));
	processor->SetHandler([](string name) {cout << "Hello " << name << endl; });
	processor->ReceiveMessage(string("John"));
	processor->SetHandler([](string name) {cout << "Goodbye " << name << endl; });
	processor->ReceiveMessage(string("Bill"));

	cout << "End. Press anything to continue." << endl;
	char k;
	cin >> k;
	return 0;
}

