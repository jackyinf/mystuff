#include "stdafx.h"
#include "learningLambdas.h"

///////////////////////////////////////
/////////////////////////////////////// ADDRESS BOOK
///////////////////////////////////////
AddressBook::AddressBook()
{
	m_addresses = vector<string>();
	m_addresses.push_back("test.org");
	m_addresses.push_back("test.com");
}

vector<string> AddressBook::FindMatchingAddresses(function<bool(const string&)> func)
{
	vector<string> results;
	for (auto itr = m_addresses.begin(), end = m_addresses.end(); itr < end; ++itr)
	{
		if (func(*itr))
		{
			results.push_back(*itr);
		}
	}
	return results;
}

vector<string> AddressBook::FindAddressesFromOrgs()
{
	return FindMatchingAddresses([](const string& addr){ return addr.find(".org") != string::npos; });
}

///////////////////////////////////////
/////////////////////////////////////// EMAIL PROCESSOR
///////////////////////////////////////
EmailProcessor::EmailProcessor() {}

void EmailProcessor::ReceiveMessage(string& message)
{
	if (m_handler)
	{
		m_handler(message);
	}
	else
	{
		cout << "No hanlde" << endl;
	}
}

void EmailProcessor::SetHandler(function<void(const string&)> handler_func)
{
	m_handler = handler_func;
}