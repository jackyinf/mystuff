﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Moq;
using NUnit.Framework;

namespace FileManipulation
{
    class Tests
    {
        [Test]
        [ExpectedException(typeof(NullReferenceException))]
        public void WebFetcher_TestWithNullDocument_ShouldThrowException()
        {
            // Setup
            var webClientWrapper = new Mock<IWebClientWrapper>();
            webClientWrapper.Setup(x => x.FetchXml(It.IsAny<string>())).Returns((XmlDocument)null);

            var webFetcher = new Program
            {
                WebClientWrapper = webClientWrapper.Object
            };

            // Execute
            webFetcher.FetchData("http://www.somewhere.com/validUrl/");
        }

        [Test]
        public void WebFetcher_TestWithValidExampleData_ShouldReturnXmlDocument()
        {
            // Setup
            var document = new XmlDocument();
            document.LoadXml("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><names><name>Brian</name><name>John</name></names>");

            var webClientWrapper = new Mock<IWebClientWrapper>();
            webClientWrapper.Setup(x => x.FetchXml(It.IsAny<string>())).Returns(document);

            var webFetcher = new Program
            {
                WebClientWrapper = webClientWrapper.Object
            };

            // Execute
            var list = webFetcher.FetchData("http://www.somewhere.com/validUrl/");

            // Assert
            Assert.IsNotNull(list);
            Assert.IsInstanceOf(typeof(List<string>), list);
            Assert.IsTrue(list.Count != 0);
            Assert.IsTrue(list.Contains("Brian"));
            Assert.IsTrue(list.Contains("John"));
        }

        [Test]
        public void GetFilestreamOfFile()
        {
            const string pathSource = "C:/Users/jevgenir/Desktop/test.zip";
            const string pathNew = "C:/Users/jevgenir/Desktop/newtest.zip";
            using (var fsSource = new FileStream(pathSource, FileMode.Open, FileAccess.Read))
            {
                // Read the source file into a byte array. 
                byte[] bytes = new byte[fsSource.Length];
                int numBytesToRead = (int)fsSource.Length;
                int numBytesRead = 0;
                while (numBytesToRead > 0)
                {
                    // Read may return anything from 0 to numBytesToRead. 
                    int n = fsSource.Read(bytes, numBytesRead, numBytesToRead);

                    // Break when the end of the file is reached. 
                    if (n == 0)
                        break;

                    numBytesRead += n;
                    numBytesToRead -= n;
                }
                numBytesToRead = bytes.Length;

                File.WriteAllLines(@"C:\Users\jevgenir\Desktop\newtest.txt", bytes.Select(b => b.ToString()).ToList());

                // Write the byte array to the other FileStream. 
                using (FileStream fsNew = new FileStream(pathNew,
                    FileMode.Create, FileAccess.Write))
                {
                    fsNew.Write(bytes, 0, numBytesToRead);
                }
            }
        }

    }
}
