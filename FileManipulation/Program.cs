﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FileManipulation
{
    public class Program
    {
        static void Main(string[] args)
        {


        }

        public List<string> FetchData(string url)
        {
            // Get XmlDocument from the WebClient wrapper class
            var document = WebClientWrapper.FetchXml(url);

            // Go through the document and aggregate a list of strings
            var list = new List<string>();
            foreach (var node in document.GetElementsByTagName("name"))
            {
                list.Add(((XmlNode)node).InnerText);
            }

            return list;
        }

        /// <summary>
        /// Loosely-coupled wrapper
        /// </summary>
        private IWebClientWrapper _webClientWrapper;
        public IWebClientWrapper WebClientWrapper
        {
            get { return _webClientWrapper ?? (_webClientWrapper = new WebClientWrapper()); }
            set { _webClientWrapper = value; }
        }
    }

    /// <summary>
    /// Interface for injection
    /// </summary>
    public interface IWebClientWrapper
    {
        XmlDocument FetchXml(string url);
    }

    /// <summary>
    /// Implementation
    /// </summary>
    public class WebClientWrapper : IWebClientWrapper
    {
        public XmlDocument FetchXml(string url)
        {
            var document = new XmlDocument();
            document.Load(new WebClient().OpenRead(url));
            return document;
        }
    }


}
