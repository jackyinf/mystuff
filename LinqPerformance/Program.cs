﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LinqPerformance
{
    class Program
    {
        static void Main(string[] args)
        {
            // test1
            var start = DateTime.Now;
            Console.WriteLine("Sync");
            var numberList = Enumerable.Range(1, 100);
            var queryB = from num in numberList
                         select ExpensiveFunction(num); //good for PLINQ
            Console.WriteLine(queryB.Count(c => c != 999));
            Console.WriteLine("Time Spent: " + (DateTime.Now.Second - start.Second));
            Console.ReadKey();

            // test2
            start = DateTime.Now;
            Console.WriteLine("Async");
            numberList = Enumerable.Range(1, 100);
            var queryA = from num in numberList.AsParallel()
                         select ExpensiveFunction(num); //good for PLINQ
            Console.WriteLine(queryA.Count(c => c != 999));
            Console.WriteLine("Time Spent: " + (DateTime.Now.Second - start.Second));
            Console.ReadKey();
        }

        private static int ExpensiveFunction(int num)
        {
            Console.WriteLine("EF: " + num);
            Thread.Sleep(100);
            return num%2 != 0 ? num : 999;
        }
    }
}
