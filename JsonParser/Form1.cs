﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace JsonParser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonParse_Click(object sender, EventArgs e)
        {
            try
            {
                string json = textBoxSource.Text.Trim();
                json = "[" + json;
                json = json.Replace(",,", ",");
                json = json.Replace("))", ")");
                json = json.Replace("((", "(");
                json = json.Replace("ObjectId(", "");
                json = json.Replace("ISODate(", "");
                json = json.Replace("LUUID(", "");
                json = json.Replace("Date(", "");
                json = json.Replace("NumberLong(", "");
                json = json.Replace("null", "\"null\""); // 
                json = json.Replace("\")", "\"");
                json = json.Replace("(\"", "\"");
                json = json.Replace("0)", "0");
                json = json.Replace("}\r\n", "},\r\n");
                json = json + "]";
                json = json.Replace(",\r\n]", "\r\n]");
                json = Regex.Replace(json, "/.*/", "");
                //json += "{\"finish\": \"finish\"}]";
                var serializer = new JavaScriptSerializer();
                serializer.RegisterConverters(new[] {new DynamicJsonConverter()});
                dynamic results = serializer.Deserialize(json, typeof (List<object>));

                using (var table = new DataTable())
                {
                    foreach (var result in results)
                    {
                        var bars = result._dictionary;
                        var values = new List<object>();
                        foreach (var bar in bars)
                        {
                            if (!table.Columns.Contains(bar.Key))
                                table.Columns.Add(bar.Key);
                            values.Add(bar.Value.ToString());
                        }
                        table.Rows.Add(values.ToArray());

                    }
                    dataGridViewDestination.DataSource = table;
                }
                //labelError.Visible = false;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                //labelError.Visible = true;
                //labelError.Text = exception.Message;
            }
        }
    }
}
