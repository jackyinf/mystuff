﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace AutoMapperTutorial
{
    class Program
    {
        static void Main(string[] args)
        {
            var thing1 = new MyThing1 { Name = "test", Value = "123", Number = "123" };
            Mapper.CreateMap<MyThing1, MyThing2>();
            var thing2 = Mapper.Map<MyThing2>(thing1);

            Console.WriteLine(@"Name: {0}, Value: {1}, Cow: {2}", thing2.Name, thing2.Value, thing2.Number);
            Console.ReadKey();
        }
    }

    class MyThing1
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Number { get; set; }
        
    }

    class MyThing2
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public int Number { get; set; }
    }
}
