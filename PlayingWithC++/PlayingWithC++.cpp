// PlayingWithC++.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

class Test{
public:
	Test::Test(int x);
	Test::~Test();
	int GetABC();
private:
	int abc = 0;
};

Test::Test(int x)
{
	abc = x;
}

Test::~Test(){
	abc = -1;
}

int Test::GetABC()
{
	return Test::abc;
}

void haha(int *test){
	*test = 34;
}

int _tmain(int argc, _TCHAR* argv[])
{
	int i = 3;
	int* ptr_to_i = &i;
	int** ptr_to_ptr_to_i = &ptr_to_i;

	cout << **ptr_to_ptr_to_i << ", " << *ptr_to_i << ", " << &i << endl;

	int* a = new int(44);
	cout << *a << endl;

	int x = 4;
	haha(&x);
	cout << x << endl;

	Test* test = new Test(42);
	cout << test->GetABC() << endl;
	delete test;
	cout << test << endl;

	cin.get();
	return 0;
}

