﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using NUnit.Mocks;

namespace HelloWorld
{
    public class Person
    {
        public string Id;
        public string FirstName;
        public string LastName;

        public Person(string newId, string fn, string ln)
        {
            Id = newId;
            FirstName = fn;
            LastName = ln;
        }
    }

    public interface IPersonRepository
    {
        List<Person> GetPeople();
        int GetNumber();
        Person GetPersonById(string id);
    }

    public class PersonService
    {
        private readonly IPersonRepository personRepos;

        public PersonService(IPersonRepository repos)
        {
            personRepos = repos;
        }

        public List<Person> GetAllPeople()
        {
            return personRepos.GetPeople();
        }

        public List<Person> GetAllPeopleSorted()
        {
            List<Person> people = personRepos.GetPeople();
            people.Sort(delegate(Person lhp, Person rhp) { return lhp.LastName.CompareTo(rhp.LastName); });
            return people;
        }

        public Person GetPerson(string id)
        {
            try
            {
                return personRepos.GetPersonById(id);
            }
            catch (ArgumentException)
            {
                return null; // no person with that id was found
            }
        }
    }

    [TestFixture]
    public class MockTutorial
    {
        // The dynamic mock proxy that we will use to implement IPersonRepository
        private DynamicMock personRepositoryMock;
        // Set up some testing data
        private readonly Person onePerson = new Person("1", "Wendy", "Whiner");
        private readonly Person secondPerson = new Person("2", "Aaron", "Adams");
        private List<Person> peopleList;

        [SetUp]
        public void TestInit()
        {
            peopleList = new List<Person> {onePerson, secondPerson};
            // Construct a Mock Object of the IPersonRepository Interface
            personRepositoryMock = new DynamicMock(typeof(IPersonRepository));
        }

        [Test]
        public void TestGetAllPeople()
        {
            // Tell that mock object when the "GetPeople" method is
            // called to return a predefined list of people
            personRepositoryMock.ExpectAndReturn("GetPeople", peopleList);
            // Construct a Person service with the Mock IPersonRepository
            PersonService service = new PersonService(
                (IPersonRepository) personRepositoryMock.MockInstance);
            // Call methods and assert tests
            Assert.AreEqual(2, service.GetAllPeople().Count);
        }

        [Test]
        public void TestGetNumber()
        {
            personRepositoryMock.ExpectAndReturn("GetNumber", 8);
            var instance = (IPersonRepository) personRepositoryMock.MockInstance;
            var number = instance.GetNumber();
            Assert.AreEqual(8, number);
        }

        [Test]
        public void TestGetAllPeopleSorted()
        {
            // Tell that mock object when the "GetPeople" method is called to
            // return a predefined list of people
            personRepositoryMock.ExpectAndReturn("GetPeople", peopleList);
            PersonService service = new PersonService(
                (IPersonRepository) personRepositoryMock.MockInstance);
            // This method really has "business logic" in it - the sorting of people
            List<Person> people = service.GetAllPeopleSorted();
            Assert.IsNotNull(people);
            Assert.AreEqual(2, people.Count);
            // Make sure the first person returned is the correct one
            Person p = people[0];
            Assert.AreEqual("Adams", p.LastName);
        }

        [Test]
        public void TestGetSinglePersonWithValidId()
        {
            // Tell that mock object when the "GetPerson" method is called to
            // return a predefined Person
            personRepositoryMock.ExpectAndReturn("GetPersonById", onePerson, "1");
            PersonService service = new PersonService(
                (IPersonRepository) personRepositoryMock.MockInstance);
            Person p = service.GetPerson("1");
            Assert.IsNotNull(p);
            Assert.AreEqual(p.Id, "1");
        }

        [Test]
        public void TestGetSinglePersonWithInalidId()
        {
            // Tell that mock object when the "GetPersonById" is called with a null
            // value to throw an ArgumentException
            personRepositoryMock.ExpectAndThrow("GetPersonById",
                new ArgumentException("Invalid person id."), null);
            PersonService service = new PersonService(
                (IPersonRepository) personRepositoryMock.MockInstance);
            // The only way to get null is if the underlying IPersonRepository
            // threw an ArgumentException
            Assert.IsNull(service.GetPerson(null));
        }
    }
}