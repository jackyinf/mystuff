﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AspMvcIntegrationTests.Startup))]
namespace AspMvcIntegrationTests
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
