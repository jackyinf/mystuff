﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using BLToolkit.Data;
using BLToolkit.Data.Linq;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using BLToolkit.Mapping.CustomObjects;

namespace Postgre
{
    class Program
    {
        //private const string ConnectionString = "User ID=postgres;Password=123;Server=localhost;Database=postgres;";

        static void Main(string[] args)
        {
            // 3rd party data provider registration.
            //
            DbManager.AddDataProvider(new PostgreSQLDataProvider());

            //DbManager.AddConnectionString(
            //    "PostgreSQL",          // Provider name
            //    "PostgreSQL",          // Configuration
            //    ConnectionString); // Connection string
             
            //*

            const bool @select = true;
            const bool add = true;
            const bool update = false;

            if (add)
            {
                // ADD
                using (var db = new SaisDb())
                {
                    //var course = new CourseEntity { Name = "Andmebaasid 2", Attributes = new CourseEntityAttributes { code = "123", description = "test", institution = "institution1"} };
                    //new SqlQuery<CourseEntity>().Insert(db, course);
                    var somethingEntity = new SomeEntity {Name = "Test5", Stuff = new SomeEntityAttribute {Name = "123"}, Test = new AnotherEntityAttribute{ Number = "555"}};
                    new SqlQuery<SomeEntity>().Insert(db, somethingEntity);
                }
            }

            if (update)
            {
                // UPDATE
                using (var db = new SaisDb())
                {
                    //var course = new CourseEntity
                    //{
                    //    ID = 22,
                    //    Name = "Objektorienteeritud Java",
                    //    Attributes = new CourseEntityAttributes { code = "14141", description = "this is Java", institution = "TTU" }
                    //};
                    //new SqlQuery<CourseEntity>().Update(db, course);
                    var somethingEntity = new SomeEntity { ID = 2, Name = "Test2", Stuff = new SomeEntityAttribute { Name = "12345" } };
                    new SqlQuery<SomeEntity>().Update(db, somethingEntity);
                }
            }

            if (select)
            {
                // GET
                using (var db = new SaisDb())
                {
                    //var courses = db.Course.ToList();
                    var something = db.Some.ToList();

                    //var courses = new List<CourseEntity>();
                    //new SqlQuery<CourseEntity>().SelectAll(db, courses);
                    //var courses = db.SetCommand("SELECT id, name, attributes FROM courses").ExecuteList<CourseEntity>();
                }
            }
        }
    }

    [TableName("courses")]
    class CourseEntity
    {
        [MapField("id"), PrimaryKey, Identity]
        public int ID { get; set; }

        [MapField("name")]
        public string Name { get; set; }

        [MapField("attributes")]
        public CourseEntityAttributes Attributes { get; set; }

    }

    [TableName("something")]
    class SomeEntity
    {
        [MapField("id"), PrimaryKey, Identity]
        public int ID { get; set; }

        [MapField("name")]
        public string Name { get; set; }

        [MapField("stuff")]
        public SomeEntityAttribute Stuff { get; set; }

        [MapField("test")]
        public AnotherEntityAttribute Test { get; set; }
    }

    public class SomeEntityAttribute : Hstore
    {
        public string Name { get; set; }
    }

    public class AnotherEntityAttribute : Hstore
    {
        public string Number { get; set; }
    }
}
