﻿using BLToolkit.Data;
using BLToolkit.Data.Linq;

namespace Postgre
{
    class SaisDb : DbManager
    {
        public SaisDb()
            : base("SAIS.Data.Configuration.Db")
        {
        }

        public Table<CourseEntity> Course { get { return GetTable<CourseEntity>(); } }
        public Table<SomeEntity> Some { get { return GetTable<SomeEntity>(); } }
    }
}
