﻿/// <reference path="jquery-1.10.2.js" />
/// <reference path="angular.js" />
/// <reference path="jasmine.js" />
/// <reference path="angular-mocks.js" />
/// <reference path="app.js" />
/// <reference path="code.js" data-cover />

//test("will add 5 to number", function () {
//    var res = mathLib.add5(10);

//    equal(res, 15, "should add 5");
//});

//test("will add 6 to number", function () {
//    var res = mathLib.add6(10);

//    equal(res, 16, "should add 6");
//});

describe("DataEntryCtrl", function () {
    var element;
    var $scope;
    beforeEach(inject(function($compile, $rootScope) {
        $scope = $rootScope;
        element = angular.element("<div>{{2 + 3}}</div>");
        element = $compile(element)($rootScope);
    }));

    it("shoud equal five", function() {
        $scope.$digest();
        expect(element.html()).toEqual("5");
    });
    
    it("oneEqualsOne_ReturnTrue", function () {
        expect(32).toEqual(32);
    });
});

describe("Testing AngularJS App", function() {
    var $scope1 = null;
    var $controller = null;
    var $httpBackend = null;
    var ctrl = null;
    
    beforeEach(function () {
        module("app");
    });


    beforeEach(function () {// we need to use toEqualData because the Resource has extra properties 
        // which make simple .toEqual not work.
        this.addMatchers({
            toEqualData: function (expect) {
                return angular.equals(expect, this.actual);
            }
        });
    });

    //you need to indicate your module in a test


    // read: http://docs.angularjs.org/api/ngMock.$httpBackend
    beforeEach(inject(function ($rootScope, _$controller_, _$httpBackend_) {
        $scope1 = $rootScope.$new();
        $controller = _$controller_;
        $httpBackend = _$httpBackend_;

        // Fake all my services
        $httpBackend.whenGET("api/location").respond([]);
        $httpBackend.whenGET("api/tuitionType").respond([]);
        $httpBackend.whenGET("api/cohort").respond([]);
        $httpBackend.whenGET("api/course").respond([]);

        ctrl = $controller('lalala', { $scope: $scope1 });

    }));
    
    // Actual Unit Tests
    // Naming: Method_Scenario_ExpectedBehavior

    it("oneEqualsOne_ReturnTrue", function () {
        expect(1).toEqual(1);
    });

    it("blabla_ReturnTrue", function() {
        expect($scope1.blabla).toEqual(30);
    });

    it("httpBackend test1", function() {

    });

});

// ----------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------

// testing controller
describe('MyController', function () {
    var $httpBackend, $rootScope, createController;

    beforeEach(inject(function ($injector) {
        // Set up the mock http service responses
        $httpBackend = $injector.get('$httpBackend');
        // backend definition common for all tests
        $httpBackend.when('GET', '/auth.py').respond({ userId: 'userX' }, { 'A-Token': 'xxx' });

        // Get hold of a scope (i.e. the root scope)
        $rootScope = $injector.get('$rootScope');
        // The $controller service is used to create instances of controllers
        var $controller = $injector.get('$controller');

        createController = function () {
            return $controller('MyController', { '$scope': $rootScope });
        };
    }));


    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    it('should fetch authentication token', function () {
        $httpBackend.expectGET('/auth.py');
        var controller = createController();
        $httpBackend.flush();
    });


    it('should send msg to server', function () {
        var controller = createController();
        $httpBackend.flush();

        // now you don’t care about the authentication, but
        // the controller will still send the request and
        // $httpBackend will respond without you having to
        // specify the expectation and response for this request

        $httpBackend.expectPOST('/add-msg.py', 'message content').respond(201, '');
        $rootScope.saveMessage('message content');
        expect($rootScope.status).toBe('Saving...');
        $httpBackend.flush();
        expect($rootScope.status).toBe('');
    });


    it('should send auth header', function () {
        var controller = createController();
        $httpBackend.flush();

        $httpBackend.expectPOST('/add-msg.py', undefined, function (headers) {
            // check if the header was send, if it wasn't the expectation won't
            // match the request and the test will fail
            return headers['Authorization'] == 'xxx';
        }).respond(201, '');

        $rootScope.saveMessage('whatever');
        $httpBackend.flush();
    });
});