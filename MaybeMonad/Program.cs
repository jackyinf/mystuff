﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monads.NET;

namespace MaybeMonad
{
    internal class Program
    {
        /// <summary>
        /// https://github.com/phlik/Monads.net
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            var names = new List<string>{"John", "Ben"};
            //names = null;
            names = names.Do(list => list.Sort());

            Console.Write(names);

            Console.ReadKey();
        }
    }



}
