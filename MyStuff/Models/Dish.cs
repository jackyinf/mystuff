﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyStuff.Models
{
    public class Dish : IValidatableObject
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(30)]
        [KeelaRopendamine]
        public string Name { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Name == null || Name.Trim().Equals(string.Empty))
                yield return new ValidationResult("ei tohi olla tyhi");
            if (Name != null && Name.Contains("error"))
                yield return new ValidationResult("Palun ara kasuta sõna \"error\" :)");
            if (Name != null && Name.Contains("loll"))
                yield return new ValidationResult("Ara kasuta sõna \"loll\"!!");
        }
    }

    public class KeelaRopendamine : ValidationAttribute
    {
        private string errorMessage = "";
        public override bool IsValid(object value)
        {
            bool result = !((string) value != null && ((string) value).Contains("kurat"));
            errorMessage = "kuraaaat!!";
            return result;
        }

        public override string FormatErrorMessage(string name)
        {
            return errorMessage;
        }
    }
}