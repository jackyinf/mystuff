﻿var app = angular.module("app", []);


app.controller("MainCtrl", function($scope) {
    $scope.dataToDisplay = 5;
    console.log('sth');
    $scope.items = [];

    var counter = 0;
    $scope.loadMore = function() {
        for (var i = 0; i < 5; i++) {
            $scope.items.push({ id: counter });
            counter += 1;
            $scope.dataToDisplay += 5;
        }
    };

    $scope.reset = function() {
        $scope.dataToDisplay = 20;
    };

    $scope.loadMore();
});

app.directive('whenScrolled', function() {
    return function(scope, elm, attr) {

        var raw = elm[0];


        elm.bind('scroll', function() {
            if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                scope.$apply(attr.whenScrolled);
            }
        });
    };
});

app.directive('whenReset', function() {
    return {
        link: function(scope) {
            scope.dataToDisplay = 20;
        }
    };
});


/*

app.directive('reset_data_to_display', function() {
    return {
        restrict: "A",
        link: function (scope) {
            console.log("dsfa");
            scope.dataToDisplay = 20;
        }
    };
});

app.directive('onChange', function () {
    return {
        restrict: 'A',
        scope: { 'onChange': '=' },
        link: function (scope) {
            console.log("onchange has been initiated");
            scope.dataToDisplay = 20;
        }
    };
});

*/