using HelloNancy.Objects;
using Nancy;

namespace HelloNancy.Modules
{
    public class HomeModule : NancyModule
    {
        public HomeModule()
        {
            Get["/"] = parameters =>
            {
                var blogPost = new BlogPost
                {
                    ID = 1,
                    Name = "John"
                };

                return View["Index", blogPost];
            };
        }
    }
}