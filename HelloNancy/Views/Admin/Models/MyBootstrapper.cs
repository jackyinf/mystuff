﻿using HelloNancy.Models;
using Nancy.Bootstrapper;
using TinyIoCContainer = Nancy.TinyIoc.TinyIoCContainer;

namespace HelloNancy.Views.Admin.Models
{
    public class MyBootstrapper : Nancy.DefaultNancyBootstrapper
    {
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);
            container.Register<WebApplicationService>().AsSingleton();
        }
    }
}