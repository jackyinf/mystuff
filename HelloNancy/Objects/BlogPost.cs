﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelloNancy.Objects
{
    public class BlogPost
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}