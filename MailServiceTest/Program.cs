﻿using System;
using System.Net;
using System.Net.Mail;
using S22.Imap;
using S22.Pop3;

namespace MailServiceTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //string serverName = "imap.gmail.com";
            //string userName = "jevgeni.rum@gmail.com";
            //string password = "";

            //string serverName = "imap.mail.com";
            //string userName = "zekasp123@bk.ru";
            //string password = "";

            Console.WriteLine(@"Write server to connect with senditplus@kopioniini.fi and B******");
            var server = Console.ReadLine();

            /***
             * IMAP
             ***/
            //PingImap(server);

            /***
             * POP3
             ***/
            PingPop3(server);

            /***
             * SMTP
             ***/
            //Smtp();
        }

        /// <summary>
        /// 
        /// </summary>
        public static void PingImap(string serverName = "mail.finestmedia.ee", string userName = "senditplus@kopioniini.fi", string password = "BUr5G5xDk")
        {
            //string userName = @"niini\senditplus";

            Console.WriteLine("Logging in using Imap...");
            ImapClient oClient;
            try
            {
                oClient = new ImapClient(serverName, 143, userName, password, S22.Imap.AuthMethod.Login, true);
                Console.WriteLine("Authed: " + oClient.Authed);

                // List mailboxes
                foreach (var mailbox in oClient.ListMailboxes())
                {
                    Console.WriteLine(mailbox);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Failed: " + exception.Message);
                Console.WriteLine("Failed: " + exception.StackTrace);
            }

            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }

        /// <summary>
        /// 
        /// </summary>
        public static void PingPop3(string serverName = "webmail.kopioniini.fi", string userName = @"niini\senditplus", string password = "BUr5G5xDk")
        {
            Console.WriteLine("Logging in using Pop3...");
            Pop3Client popClient;
            try
            {
                popClient = new Pop3Client(serverName, 995, userName, password, S22.Pop3.AuthMethod.Login, true);
                Console.WriteLine("Authed: " + popClient.Authed);
            }
            catch (Exception exception)
            {
                Console.WriteLine("Failed: " + exception.Message);
                Console.WriteLine("Failed: " + exception.StackTrace);
            }

            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public static void Smtp(string serverName = "mail.finestmedia.ee", string userName = "senditplus@kopioniini.fi", string password = "BUr5G5xDk")
        {
            Console.WriteLine("Sending using Smtp...");
            var mailMsg = new MailMessage
            {
                DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure,
                Subject = "TestSubject333",
                Body = "TestBody333",
                From = new MailAddress(userName)
            };
            mailMsg.To.Add(new MailAddress("jevgeni.rum@gmail.com"));

            var smtpClient = new SmtpClient(serverName)
            {
                Port = 25,
                Credentials = new NetworkCredential(userName, password),
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network
            };

            try
            {
                ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;
                smtpClient.Send(mailMsg);
                Console.WriteLine("Send Success");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine(exception.StackTrace);
            }

            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }

    }
}
