﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.Data;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace BltoolkitStarter
{
    class Program
    {
        static void Main(string[] args)
        {
            using (DbManager db = new DbManager("DemoConnection"))
            {
                Console.Write(db.Connection.Database);
                var result = db.SetCommand("SELECT * FROM UserRole").ExecuteList<UserRole>();
            }

            Console.ReadKey();
        }

        public class UserRole
        {
            [MapField("ID"), PrimaryKey, NonUpdatable]
            public Guid ID;

            public int Value;
            public DateTime CreatedOn;
        }
    }
}
