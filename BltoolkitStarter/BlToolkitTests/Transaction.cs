﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLToolkit.Data;
using NUnit.Framework;

namespace BltoolkitStarter.BlToolkitTests
{
    class Transaction
    {
        [Test]
        public void Test1()
        {
            using (var db = new DbManager("DemoManager"))
            {
                db.BeginTransaction(IsolationLevel.ReadCommitted);
                db.SetCommand("SELECT * FROM UserRole").ExecuteList<Program.UserRole>();
                db.CommitTransaction();
            }
        }

    }
}
