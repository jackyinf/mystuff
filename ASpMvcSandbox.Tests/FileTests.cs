﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace ASpMvcSandbox.Tests
{
    public class FileTests
    {
        private const string Destination = @"C:\temp\kopio\test";

        [Test]
        public void CombineFilesTest_Approach1()
        {
            var directoryInfo = new DirectoryInfo(Destination);
            var inputFiles = directoryInfo.GetFiles("*.chunk*");

            var indexOfChunkseparator = inputFiles.First().FullName.LastIndexOf(".", StringComparison.Ordinal);
            var fileName = inputFiles.First().FullName.Substring(0, indexOfChunkseparator);

            using (var output = File.Create(fileName))
            {
                foreach (var file in inputFiles)
                {
                    using (var input = File.OpenRead(file.FullName))
                    {
                        var buffer = new byte[2 * 1024];
                        int bytesRead;
                        while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            output.Write(buffer, 0, bytesRead);
                        }
                    }
                }
            }
        }

        [Test]
        public void CombineFilesTest_Approach2()
        {
            // Get only first chunks of all files
            var directoryInfo = new DirectoryInfo(Destination);
            var firstChunks = directoryInfo.GetFiles("*.chunk001");

            // loop through each unique name which is to be created
            foreach (var firstChunk in firstChunks)
            {
                using (var output = File.Create(SliceName(firstChunk.FullName, ".")))
                {
                    // get all chunks of current filename
                    var inputFiles = directoryInfo.GetFiles(string.Format("{0}.{1}*", SliceName(firstChunk.Name, "."), "chunk"));
                    foreach (var file in inputFiles)
                    {
                        File.OpenRead(file.FullName).CopyTo(output);
                        
                    }
                    output.Dispose();
                }
            }

        }

        [Test]
        public void RemoveAllChunks()
        {
            // Remove all chunks
            var directoryInfo = new DirectoryInfo(Destination);
            var allChunks = directoryInfo.GetFiles("*.chunk*");
            foreach (var chunk in allChunks)
            {
                File.Delete(chunk.FullName);
            }
        }

        private string SliceName(string filename, string separator)
        {
            var indexOfChunkseparator = filename.LastIndexOf(separator, StringComparison.Ordinal);
            return filename.Substring(0, indexOfChunkseparator);
        }
    }
}
