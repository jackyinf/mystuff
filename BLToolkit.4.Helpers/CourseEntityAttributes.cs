﻿namespace PostgreEntities
{
    public class CourseEntityAttributes
    {
        public string code { get; set; }
        public string description { get; set; }
    }
}
